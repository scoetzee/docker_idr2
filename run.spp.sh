#!/bin/bash
set -e

function runIDR {
    ## Init Values
    local mySampleID1="$1"
    local mySampleID2="$2"
    local myReps1="$3"
    local myReps2="$4"
    local myPeakDir="$5"
    local myIDRDir="${myPeakDir}/idr"
    mkdir -p "${myIDRDir}"
    local sample1="${myPeakDir}/${mySampleID1}_rep${myReps1}_vs_Input.regionPeak.gz"
    local sample1_pr1="${myPeakDir}/${mySampleID1}_rep${myReps1}_pr1_vs_Input.regionPeak.gz"
    local sample1_pr2="${myPeakDir}/${mySampleID1}_rep${myReps1}_pr2_vs_Input.regionPeak.gz"
    
    local sample2="${myPeakDir}/${mySampleID2}_rep${myReps2}_vs_Input.regionPeak.gz"
    local sample2_pr1="${myPeakDir}/${mySampleID2}_rep${myReps2}_pr1_vs_Input.regionPeak.gz"
    local sample2_pr2="${myPeakDir}/${mySampleID2}_rep${myReps2}_pr2_vs_Input.regionPeak.gz"
    
    local pooled="${myPeakDir}/pooledreads_rep0_vs_Input.regionPeak.gz"
    local pooled_pr1="${myPeakDir}/pooledreads_rep0_pr1_vs_Input.regionPeak.gz"
    local pooled_pr2="${myPeakDir}/pooledreads_rep0_pr2_vs_Input.regionPeak.gz"
    ## Create Workspace
    ## run macs2
    echo "calling IDR on $mySampleID1 vs $mySampleID2"
    idr --samples <(zcat ${sample1}) <(zcat ${sample2}) \
	--peak-list <(zcat ${pooled}) \
	--plot \
    --use-best-multisummit-IDR \
	--input-file-type narrowPeak \
	--rank signal.value \
	-o ${myIDRDir}/${mySampleID1}_${mySampleID2}_idr
    idr --samples <(zcat ${sample1_pr1}) <(zcat ${sample1_pr2}) \
	--peak-list <(zcat ${sample1}) \
	--plot \
    --use-best-multisummit-IDR \
	--input-file-type narrowPeak \
	--rank signal.value \
	-o ${myIDRDir}/${mySampleID1}_selfconsist_idr
    idr --samples <(zcat ${sample2_pr1}) <(zcat ${sample2_pr2}) \
	--peak-list <(zcat ${sample2}) \
	--plot \
    --use-best-multisummit-IDR \
	--input-file-type narrowPeak \
	--rank signal.value \
	-o ${myIDRDir}/${mySampleID2}_selfconsist_idr
    idr --samples <(zcat ${pooled_pr1}) <(zcat ${pooled_pr2}) \
	--peak-list <(zcat ${pooled}) \
	--plot \
    --use-best-multisummit-IDR \
	--input-file-type narrowPeak \
	--rank signal.value \
	-o ${myIDRDir}/pooled_selfconsist_idr
    npeaks1=$(awk '$5 > 830' ${myIDRDir}/${mySampleID1}_${mySampleID2}_idr | wc -l)
    npeaks2=$(awk '$5 > 830' ${myIDRDir}/${mySampleID1}_selfconsist_idr | wc -l)
    npeaks3=$(awk '$5 > 830' ${myIDRDir}/${mySampleID2}_selfconsist_idr | wc -l)
    npeaks4=$(awk '$5 > 830' ${myIDRDir}/pooled_selfconsist_idr | wc -l)
    echo -e "rep1_vs_rep2\trep1_pr\trep2_pr\tpooled" > "${myIDRDir}/numpeaks.txt"
    echo -e "${npeaks1}\t${npeaks2}\t${npeaks3}\t${npeaks4}" >> "${myIDRDir}/numpeaks.txt"
    cat ${myIDRDir}/${mySampleID1}_${mySampleID2}_idr | awk '$5 > 830' | cut -f1-10 > ${myIDRDir}/result_idr0_01.narrowPeak
}
export -f runIDR

MANIFEST=/data/manifest.csv
IFS=","

if [[ -e $MANIFEST ]] ; then
	echo "starting with idr analysis ... "
else
	echo "directory does not contain manifest file"
	exit 1
fi

TOTALLINES=$(cat $MANIFEST | wc -l)
TOTALSAMPLES=$(cut -f1 -d',' $MANIFEST | sort -u | wc -l)

if [[ $TOTALSAMPLES -ne $TOTALLINES ]] ; then
    echo "not all sample names are unique... quitting"
    return 1
fi

cd /data
## Individual Replicates
sed '1d' $MANIFEST | while read SampleID Tissue Factor Condition Treatment Replicate Reads Input; do
    PEAKDIR="/data/output/spp/${Tissue}_${Factor}_${Condition}_${Treatment}"
    if [[ ${Replicate} -eq 1 ]] ; then
	unset IFS
	SAMPLEIDS=( $(grep "$Tissue,$Factor,$Condition,$Treatment" ${MANIFEST} | cut -f1 -d",") )
	SAMPLEREPS=( $(grep "$Tissue,$Factor,$Condition,$Treatment" ${MANIFEST} | cut -f6 -d",") )
	runIDR ${SAMPLEIDS[0]} ${SAMPLEIDS[1]} ${SAMPLEREPS[0]} ${SAMPLEREPS[1]} ${PEAKDIR}
	IFS=","
    fi
done
